<?php

class Test_api extends CI_Controller {

	function index(){
        $this->load->view('api_view');
    }

    function action(){
        if($this->input->post('data_action')){
            $data_action=$this->input->post('data_action');

            if($data_action == "Insert"){
                $api_url="http://localhost/crudapi/api/insert";
            

            $form_data=array(
                'name'    => $this->input->post('name'),
                'email'   => $this->input->post('email'),
                'telefone' => $this->input->post('telefone'),
                'estado'  => $this->input->post('estado'),
                'cidade'  => $this->input->post('cidade'),
                'nascimento' => $this->input->post('nascimento'),
                'plano'   => $this->input->post('plano')
            );

            $client = curl_init($api_url);

            curl_setopt($client, CURLOPT_POST,true);

            curl_setopt($client, CURLOPT_POSTFIELDS, $form_data);

            curl_setopt($client, CURLOPT_RETURNTRANSFER, true);

            $response = curl_exec($client);

            curl_close($client);

            echo $response;
        }

            if($data_action == "fetch_all"){
                $api_url="http://localhost/crudapi/api";

                $client=curl_init($api_url);

                curl_setopt($client, CURLOPT_RETURNTRANSFER, true);

                $response= curl_exec($client);
                curl_close($client);

                $result= json_decode($response);
                $output = '';

                if(count($result) > 0){
                    foreach($result as $row){
                        $output .='
                        <tr>
                            <td>'.$row->nome.'</td>
                            <td>'.$row->email.'</td>
                            <td>'.$row->telefone.'</td>
                            <td>'.$row->estado.'</td>
                            <td>'.$row->cidade.'</td>
                            <td>'.$row->data_nascimento.'</td>
                            <td>'.$row->plano.'</td>
                            <td><button type="button" name="edit" class="btn btn-warning btn-xs edit" id="'.$row->id.'">Editar</button</td>
                            <td><button type="button" name="delete" class="btn btn-danger btn-xs delete" id="'.$row->id.'">Deletar</button></td>
                        </tr>
                        ';
                    }
                }else{
                    $output .='
                    <tr>
                        <td colspan="4" align="center"> No Data Found</td>
                    </tr>
                    ';
                }
                echo $output;
            }
        }else{
            echo "falhou aqui.";
            var_dump($this->input->post('data_action'));
        }
    }
}

?>