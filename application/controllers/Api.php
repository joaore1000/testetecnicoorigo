<?php

class Api extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('api_model');
        $this->load->library('form_validation');
    }

    function index(){
        $data = $this->api_model->fetch_all();
        echo json_encode($data->result_array());
    }

    function insert(){
        $this->form_validation->set_rules('name','Nome','required');
        $this->form_validation->set_rules('email','E-mail','required');
        $this->form_validation->set_rules('telefone','Telefone','required');
        $this->form_validation->set_rules('estado','Estado','required');
        $this->form_validation->set_rules('cidade','Cidade','required');
        $this->form_validation->set_rules('nascimento','Data de Nascimento','required');
        $this->form_validation->set_rules('plano','Plano','required');
        $array= array();

        if($this->form_validation->run()){
            $data = array(
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'telefone' => $this->input->post('telefone'),
                'estado' => $this->input->post('estado'),
                'cidade' => $this->input->post('cidade'),
                'nascimento' => $this->input->post('nascimento'),
                'plano' => $this->input->post('plano'),
            );
            $this->api_model->insert_api($data);

            $array = array(
                'success'   => true
            );
        }else{

            $array=array(
                'error'    => true,
                'name_error' => form_error('name'),
                'email_error' => form_error('email'),
                'telefone_error' => form_error('telefone'),
                'estado_error' => form_error('estado'),
                'cidade_error' => form_error('cidade'),
                'nascimento_error' => form_error('nascimento'),
                'plano_error' => form_error('plano'),
            );
        }

        echo json_encode($array);
    }
}
