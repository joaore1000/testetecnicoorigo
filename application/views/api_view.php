
<html>
<head>
    <title>CRUD API REST by João Ré</title>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    
</head>
<body>
    <div class="container">
        <br />
        <h3 classe="text-center">CRUD API REST by João Ré</h3>
        <br />
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="panel-title">CRUD API REST - Teste Origo</h3>
                    </div>
                    <div class="col-md-6 text-right">
                        <button type="button" id="add_button" class="btn btn-info btn-xs">Adicionar</button>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <span id="success_message"></span>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>E-mail</th>
                            <th>Telefone</th>
                            <th>Estado</th>
                            <th>Cidade</th>
                            <th>Data de Nascimento</th>
                            <th>Plano</th>
                            <th colspan="2" class="text-center">Ação</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>

<div id="userModal" class="modal fade">
    <div class="modal-dialog">
        <form method="post" id="user_form">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Adicionar Cliente</h4>
                </div>
                <div class="modal-body">
                    <label>Nome</label>
                    <input type="text" name="name" id="name" class="form-control" />
                    <span id="name_error" class="text-danger"></span>
                    <br />
                    <label>E-mail</label>
                    <input type="text" name="email" id="email" class="form-control" />
                    <span id="email_error" class="text-danger"></span>
                    <br />
                    <label>Telefone</label>
                    <input type="text" name="telefone" id="telefone" class="form-control" />
                    <span id="telefone_error" class="text-danger"></span>
                    <br />
                    <label>Estado</label>
                    <input type="text" name="estado" id="estado" class="form-control" />
                    <span id="estado_error" class="text-danger"></span>
                    <br />
                    <label>Cidade</label>
                    <input type="text" name="cidade" id="cidade" class="form-control" />
                    <span id="cidade_error" class="text-danger"></span>
                    <br />
                    <label>Data de Nascimento</label>
                    <input type="text" name="nascimento" id="nascimento" class="form-control" />
                    <span id="nascimento_error" class="text-danger"></span>
                    <br />
                    <label>Plano</label>
                    <input type="text" name="plano" id="plano" class="form-control" />
                    <span id="plano_error" class="text-danger"></span>
                    <br />
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="user_id" id="user_id" />
                    <input type="hidden" name="data_action" id="data_action" value="Insert" />
                    <input type="submit" name="action" id="action" class="btn btn-success" value="Adicionar" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript" language="javascript" >
$(document).ready(function(){

    
    
    function fetch_data()
    {
        $.ajax({
            url:"http://localhost/crudapi/test_api/action",
            method:"POST",
            data:{data_action:'fetch_all'},
            success:function(data)
            {
                $('tbody').html(data);
            }
        });
    }

    fetch_data();

    $('#add_button').click(function(){
        $('#user_form')[0].reset();
        $('.modal-title').text("Adicionar Cliente");
        $('#action').val('Adicionar');
        $('#data_action').val("Insert");
        $('#userModal').modal('show');
    });

    $(document).on('submit','#user_form',function(event){
        event.preventDefault();
        $.ajax({
            url:"http://localhost/crudapi/test_api/action",
            method:"POST",
            data:$(this).serialize(),
            dataType:"json",
            success:function(data){
                if(data.success){
                    $('#user_form')[0].reset();
                    $('#userModal').modal('hide');
                    fetch_data();
                    if($('#data_action').val() == "Insert"){
                        $('#success_message').html('<div class="alert alert-success">Data Insert</div>');
                    }
                }
                if(data.error){
                    $('#name_error').html(data.name_error);
                    $('#email_error').html(data.email_error);
                    $('#telefone_error').html(data.telefone_error);
                    $('#estado_error').html(data.estado_error);
                    $('#cidade_error').html(data.cidade_error);
                    $('#nascimento_error').html(data.nascimento_error);
                    $('#plano_error').html(data.plano_error);

                }
            }
           
        })
    })

});

</script>

