<?php

class Api_model extends CI_Model {

	function fetch_all(){
        
        $this->db->order_by('id','DESC');
        return $this->db->get('clientes');
    }
    
    function insert_api($data){
        $this->db->insert('clientes',$data);
    }
}